package cl.ufro.dci.perfecthostapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication()
public class PerfecthostApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PerfecthostApiApplication.class, args);
	}

}
