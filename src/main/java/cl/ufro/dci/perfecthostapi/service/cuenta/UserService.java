package cl.ufro.dci.perfecthostapi.service.cuenta;

import cl.ufro.dci.perfecthostapi.domain.Usuario;
import cl.ufro.dci.perfecthostapi.repository.UsuarioDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService implements UserServiceImp{
    @Autowired
    UsuarioDao usuarioDao;

    @Override
    public Usuario registrarUsuario(Usuario user){
        return usuarioDao.save(user);
    }

    @Override
    public Iterable<Usuario> listarCuentas(){
        return usuarioDao.findAll();
    }

    @Override
    public Optional<Usuario> findByUsername(String username){
        if(usuarioDao.findByUsuNombre(username).isPresent()){
            return usuarioDao.findByUsuNombre(username);
        }else{
            return Optional.empty();
        }
    }

    @Override
    public Usuario findById(Long id){
        return usuarioDao.findById(id).orElse(null);
    }

    @Override
    public void borrarCuenta(Long id){
        Optional<Usuario> cuentaOptional = usuarioDao.findById(id);
        if(cuentaOptional.isPresent()){
            usuarioDao.deleteById(id);
        }
    }

    @Override
    public void deleteAll() {
        usuarioDao.deleteAll();
    }

}