package cl.ufro.dci.perfecthostapi.service.anuncios;

import cl.ufro.dci.perfecthostapi.domain.Anuncio;
import cl.ufro.dci.perfecthostapi.domain.Disponibilidad;
import cl.ufro.dci.perfecthostapi.repository.AnfitrionDao;
import cl.ufro.dci.perfecthostapi.repository.AnuncioDao;
import cl.ufro.dci.perfecthostapi.repository.DisponibilidadDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AnuncioService {
    @Autowired
    private AnuncioDao anuncioDao;
    @Autowired
    private DisponibilidadDao disponibilidadDao;
    @Autowired
    private AnfitrionDao anfitrionDao;

    public Anuncio guardarAnuncio(Anuncio anuncio){
        return anuncioDao.save(anuncio);
    }
    public Anuncio obtenerAnuncio(Long id){
        if (anuncioDao.findById(id).isPresent()){
            return anuncioDao.findById(id).get();
        }else{
            return null;
        }
    }
    public void borrarAnuncio(long id){
        Optional<Anuncio>anuncioOptional=anuncioDao.findById(id);
        if(anuncioOptional.isPresent()){
            anuncioDao.deleteById(id);
        }
    }
    public Anuncio editarAnuncio(Anuncio anuncio, long id){
        Optional<Anuncio>anuncioOptional=anuncioDao.findById(id);
        if(anuncioOptional.isPresent()){
            anuncioOptional.get().setAnuCiudad(anuncio.getAnuCiudad());
            anuncioOptional.get().setAnuDireccion(anuncio.getAnuDireccion());
            anuncioOptional.get().setAnuTitulo(anuncio.getAnuTitulo());
            anuncioOptional.get().setAnuPais(anuncio.getAnuPais());
            anuncioOptional.get().setAnuDescuento(anuncio.getAnuDescuento());
            anuncioOptional.get().setAnuPrecioDia(anuncio.getAnuPrecioDia());
            return anuncioDao.save(anuncioOptional.get());
        }else {
            return null;
        }
    }
    public List<Anuncio> obtenerAnuncios(){
        return anuncioDao.findAll();
    }
    public List<Anuncio>obtenerADisponible(Disponibilidad disponibilidad){
        return anuncioDao.findByFechasDisponibles(disponibilidad);
    }
}
