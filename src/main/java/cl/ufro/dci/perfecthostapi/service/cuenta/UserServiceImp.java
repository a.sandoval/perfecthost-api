package cl.ufro.dci.perfecthostapi.service.cuenta;

import cl.ufro.dci.perfecthostapi.domain.Usuario;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public interface UserServiceImp {
    Usuario registrarUsuario(Usuario user);
    Iterable<Usuario> listarCuentas();
    Optional<Usuario> findByUsername(String username);
    Usuario findById(Long id);
    void borrarCuenta(Long id);
    void deleteAll();
}
