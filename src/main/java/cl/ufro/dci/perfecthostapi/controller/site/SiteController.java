package cl.ufro.dci.perfecthostapi.controller.site;

import cl.ufro.dci.perfecthostapi.domain.Administrador;
import cl.ufro.dci.perfecthostapi.domain.Usuario;
import cl.ufro.dci.perfecthostapi.service.cuenta.UserServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("")
class SiteController {
    @Autowired
    private UserServiceImp userService;

    @Autowired
    PasswordEncoder encoder;

    @GetMapping
    public String index() {
        /*Usuario admin = new Administrador();
        admin.setUsuNombre("administrador");
        admin.setUsuClave(encoder.encode("administrador"));
        userService.registrarUsuario(admin);*/
        return "<h1>Bienvenido a PerfectHost</h1>";
    }
}