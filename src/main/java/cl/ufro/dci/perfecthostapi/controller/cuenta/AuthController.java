package cl.ufro.dci.perfecthostapi.controller.cuenta;

import cl.ufro.dci.perfecthostapi.domain.Administrador;
import cl.ufro.dci.perfecthostapi.domain.Anfitrion;
import cl.ufro.dci.perfecthostapi.domain.Huesped;
import cl.ufro.dci.perfecthostapi.domain.Usuario;
import cl.ufro.dci.perfecthostapi.security.dto.*;
import cl.ufro.dci.perfecthostapi.security.service.UserDetailsImpl;
import cl.ufro.dci.perfecthostapi.security.service.UserDetailsServiceImp;
import cl.ufro.dci.perfecthostapi.security.util.JWTUtil;
import cl.ufro.dci.perfecthostapi.service.cuenta.UserServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@Controller
@RequestMapping("/api")
@ComponentScan("repository")
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsServiceImp userDetailsService;

    @Autowired
    private JWTUtil jwtUtil;

    @Autowired
    private UserServiceImp userService;

    @Autowired
    PasswordEncoder encoder;

    private String errorUsername = "El nombre de usuario tiene que ser de minimo 5 caracteres";
    private String errorPass = "La contraseña tiene que ser de minimo 6 caracteres";

    @PostMapping("/auth/login")
    public ResponseEntity<Object> authenticateUser(@RequestBody AuthenticationRequest loginRequest) {
        Iterable<Usuario> iterable = userService.listarCuentas();
        List<Usuario> usuariosBD = new ArrayList<>();
        iterable.forEach(usuariosBD::add);
        boolean usuarioIncorrecto = false;

        for (Usuario userBD: usuariosBD) {
            System.out.println(userBD.getUsuNombre());
            if(userBD.getUsuNombre().equalsIgnoreCase(loginRequest.getUsername())
                    || userBD.getUsuClave().equals(loginRequest.getPassword())){
                 usuarioIncorrecto = true;
            }
        }
        if (usuarioIncorrecto){
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword())); //import

            SecurityContextHolder.getContext().setAuthentication(authentication);

            String jwt = jwtUtil.generateJwtToken(authentication);

            UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
            List<String> roles = userDetails.getAuthorities().stream()
                    .map(item -> item.getAuthority())
                    .collect(Collectors.toList());

            return ResponseEntity.ok(new JwtResponse(jwt,
                    userDetails.getId(),
                    userDetails.getUsername(),
                    roles));
        }else{
            return ResponseEntity.badRequest().body(new MessageResponse("Usuario y/o contraseña incorrecta"));
        }
    }

    @PostMapping("/auth/register/anfitrion")
    public ResponseEntity<Object> registrarAnfitrion(@RequestBody Usuario user){
        if(user.getUsuNombre().length() >= 5){
            if(user.getUsuClave().length() >= 6){
                Iterable<Usuario> iterable = userService.listarCuentas();
                List<Usuario> usuariosBD = new ArrayList<>();
                iterable.forEach(usuariosBD::add);

                for (Usuario userBD: usuariosBD) {
                    if(userBD.getUsuNombre().equalsIgnoreCase(user.getUsuNombre())){
                        return ResponseEntity.badRequest().body(new MessageResponse("El nombre de usuario ya existe"));
                    }
                }

                Usuario userAnfitrion=new Anfitrion();
                userAnfitrion.setUsuNombre(user.getUsuNombre());
                userAnfitrion.setUsuClave(encoder.encode(user.getUsuClave()));

                return  ResponseEntity.ok(userService.registrarUsuario(userAnfitrion));
            }else{
                return ResponseEntity.badRequest().body(new MessageResponse(this.errorPass));
            }
        }else{
            return ResponseEntity.badRequest().body(new MessageResponse(this.errorUsername));
        }
    }

    @PostMapping("/auth/register/huesped")
    public ResponseEntity<Object> registrarHuesped(@RequestBody Usuario user){
        if(user.getUsuNombre().length() >= 5){
            if(user.getUsuClave().length() >= 6){
                Iterable<Usuario> iterable = userService.listarCuentas();
                List<Usuario> usuariosBD = new ArrayList<>();
                iterable.forEach(usuariosBD::add);

                for (Usuario userBD: usuariosBD) {
                    if(userBD.getUsuNombre().equalsIgnoreCase(user.getUsuNombre())){
                        return ResponseEntity.badRequest().body(new MessageResponse("El nombre de usuario ya existe"));
                    }
                }

                Usuario userHuesped=new Huesped();
                userHuesped.setUsuNombre(user.getUsuNombre());
                userHuesped.setUsuClave(encoder.encode(user.getUsuClave()));

                return ResponseEntity.ok(userService.registrarUsuario(userHuesped));
            }else{
                return ResponseEntity.badRequest().body(new MessageResponse(this.errorPass));
            }
        }else{
            return ResponseEntity.badRequest().body(new MessageResponse(this.errorUsername));
        }
    }

    @PostMapping("/auth/register/admin")
    public ResponseEntity<Object> registrarAdmin(@RequestBody Usuario user) {
        Usuario admin = new Administrador();
        admin.setUsuNombre(user.getUsuNombre());
        admin.setUsuClave(encoder.encode(user.getUsuClave()));
        return ResponseEntity.ok(userService.registrarUsuario(admin));
    }

    @PostMapping("/auth/changerol/{name}")
    public Usuario cambioRol(@PathVariable String name){
        Usuario user = userService.findByUsername(name).get();
        Usuario userChangeRol = null;

        if(user == null){
            return null;
        }else {
            if(user instanceof Huesped){
                userChangeRol = new Anfitrion();
                userChangeRol.setUsuId(user.getUsuId());
                userChangeRol.setUsuNombre(user.getUsuNombre());
                userChangeRol.setUsuClave(user.getUsuClave());

                userService.borrarCuenta(user.getUsuId());
                userService.registrarUsuario(userChangeRol);
            }
            if(user instanceof Anfitrion){
                userChangeRol = new Huesped();
                userChangeRol.setUsuId(user.getUsuId());
                userChangeRol.setUsuNombre(user.getUsuNombre());
                userChangeRol.setUsuClave(user.getUsuClave());

                userService.borrarCuenta(user.getUsuId());
                userService.registrarUsuario(userChangeRol);
            }
            return userChangeRol;
        }
    }

    @GetMapping("/auth/user")
    public Iterable<Usuario> listarUser(){
        return userService.listarCuentas();
    }

    @PutMapping("/auth/update/{id}")
    public Usuario update2(@PathVariable long id, @RequestBody AuthenticationRequest updateUser){
        if(updateUser.getUsername().length() >= 5 && updateUser.getPassword().length() >= 6) {
            Usuario user = userService.findById(id);
            user.setUsuNombre(updateUser.getUsername());
            user.setUsuClave(encoder.encode(updateUser.getPassword()));
            return userService.registrarUsuario(user);
        }
        return null;
    }

    @DeleteMapping("/auth/delete/{name}")
    public ResponseEntity<Object> delete(@PathVariable String name){
        Usuario userDelete = userService.findByUsername(name).get();
        if (userDelete==null){
            return ResponseEntity.badRequest().body(new MessageResponse("El usuario no existe"));
        }
        userService.borrarCuenta(userDelete.getUsuId());
        return ResponseEntity.ok("Usuario Eliminado Correctamente");
    }

    @PatchMapping("/auth/recuperate/{name}")
    public ResponseEntity<Object> recuperate(@PathVariable String name, @RequestBody String pass){
        if(pass.length() >= 6){
            Iterable<Usuario> iterable = userService.listarCuentas();
            List<Usuario> usuarios = new ArrayList<>();
            iterable.forEach(usuarios::add);

            for (Usuario userBD: usuarios) {
                if(userBD.getUsuNombre().equalsIgnoreCase(name)){
                    Usuario userRecuperate = userBD;
                    if(encoder.matches(pass, userRecuperate.getUsuClave())){
                        return ResponseEntity.badRequest().body(new MessageResponse("La contraseña nueva no tiene que ser igual a la anterior!"));
                    }

                    userRecuperate.setUsuClave(encoder.encode(pass));

                    userService.registrarUsuario(userRecuperate);
                    return ResponseEntity.ok("Cuenta recuperada correctamente!");
                }
            }
            return ResponseEntity.badRequest().body(new MessageResponse("No se ha encontrado la cuenta"));
        }else{
            return ResponseEntity.badRequest().body(new MessageResponse(this.errorPass));
        }
    }

    @GetMapping("/auth/buscar/{id}")
    public ResponseEntity<Object> getFindById(@PathVariable Long id) {
        Usuario user = userService.findById(id);
        if(user == null){
            return ResponseEntity.badRequest().body(new MessageResponse("Usuario no existe"));
        }else {
            return ResponseEntity.ok(user);
        }
    }

    @PostMapping("/auth/token")
    public boolean tokenVerificado(@RequestBody String token){
        return jwtUtil.validateJwtToken(token);
    }
}