package cl.ufro.dci.perfecthostapi.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "estadoreserva")
public class EstadoReserva {
	@Id
	@Column (
		name = "est_id",
		nullable = false,
		length = 20
	)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long estId;
	@OneToMany (
		cascade = CascadeType.ALL,
		orphanRemoval = true
	)
	@JoinColumn(name = "est_id")
	private List<Reserva> reservas;
	@Column (
		name = "est_confirmar",
		nullable = false
	)
	private boolean estConfirmar;
}