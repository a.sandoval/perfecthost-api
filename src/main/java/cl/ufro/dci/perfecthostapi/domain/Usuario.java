package cl.ufro.dci.perfecthostapi.domain;


import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Inheritance
@DiscriminatorColumn(name = "usu_tipo_usuario")
@Entity
@Table(name = "usuario")
public class Usuario {
	@Id
	@Column (
		name = "usu_id",
		nullable = false,
		length = 20
	)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long usuId;
	@Column (
		name = "usu_nombre",
		nullable = true,
		length = 255
	)
	private String usuNombre;
	@Column (
		name = "usu_clave",
		nullable = false,
		length = 255
	)
	private String usuClave;
	@OneToMany (
		cascade = CascadeType.ALL,
		orphanRemoval = true
	)
	@JoinColumn(name = "usu_id")
	private List<HistorialLogin> historial;

	@Transient
	public String getDecriminatorValue() {
		return this.getClass().getAnnotation(DiscriminatorValue.class).value();
	}
}