package cl.ufro.dci.perfecthostapi.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;


@Entity
@Data
@Table(name = "historiallogin")
public class HistorialLogin {
	@Id
	@Column (
		name = "his_id",
		nullable = false,
		length = 20
	)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long hisId;
	@ManyToOne
	@JoinColumn(name = "usu_id")
	private Usuario usuario;
	@Column (
		name = "his_fecha_login",
		nullable = false
	)
	private Date hisFechaLogin;
}