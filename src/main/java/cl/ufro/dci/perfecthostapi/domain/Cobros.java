package cl.ufro.dci.perfecthostapi.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "cobros")
public class Cobros {
	@Id
	@Column (
		name = "cob_id",
		nullable = false,
		length = 20
	)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long cobId;
	@ManyToOne
	@JoinColumn(name = "anu_id")
	private Anuncio anuncio;
	@ManyToOne
	@JoinColumn(name = "est_id")
	public EstadoCobros estadoCobros;
	@Column (
		name = "cob_pago_reserva",
		nullable = false
	)
	private int cobPagoReserva;
	@Column(name = "cob_pago_arriendo")
	private int cobPagoArriendo;
	@Column(name = "cob_pago_publicacion")
	private int cobPagoPublicacion;
}