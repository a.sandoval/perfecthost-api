package cl.ufro.dci.perfecthostapi.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "reserva")
public class Reserva {
	@Id
	@Column (
		name = "res_id",
		nullable = false,
		length = 20
	)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long resId;
	@ManyToOne
	@JoinColumn(name = "est_id")
	private EstadoReserva estadoReserva;
	@ManyToOne
	@JoinColumn(name = "usu_id")
	private Huesped huesped;
	@ManyToOne
	@JoinColumn(name = "anu_id")
	private Anuncio anuncio;
	@Column (
		name = "res_precio_recerva",
		nullable = false
	)
	private int resPrecio;
	@Column (
		name = "res_fecha_inicio",
		nullable = false
	)
	private Date resFechaInicio;
	@Column (
		name = "res_fecha_termino",
		nullable = false
	)
	private Date resFechaTermino;
}