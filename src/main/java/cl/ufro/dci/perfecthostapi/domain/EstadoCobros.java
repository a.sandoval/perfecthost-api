package cl.ufro.dci.perfecthostapi.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "estadocobros")
public class EstadoCobros {
	@Id
	@Column (
		name = "est_id",
		nullable = false,
		length = 20
	)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long estId;
	@OneToMany (
		cascade = CascadeType.ALL,
		orphanRemoval = true
	)
	@JoinColumn(name = "est_id")
	public List<Cobros> cobros;
	@Column(name = "est_pago_reserva")
	private boolean estPagoReserva;
	@Column(name = "est_pago_arriendo")
	private boolean estPagoArriendo;
	@Column(name = "est_pago_publicacion")
	private boolean estPagoPublicacion;
}