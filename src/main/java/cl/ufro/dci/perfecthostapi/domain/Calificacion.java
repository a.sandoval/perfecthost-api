package cl.ufro.dci.perfecthostapi.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "calificacion")
public class Calificacion {
	@Id
	@Column (
		name = "cal_id",
		nullable = false,
		length = 20
	)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long calId;
	@ManyToOne
	@JoinColumn(name = "usu_id")
	private Huesped huesped;
	@ManyToOne
	@JoinColumn(name = "anu_id")
	public Anuncio anuncio;
	@Column(name = "cal_puntaje")
	private int calPuntaje;
	@Column(name = "cal_comentario")
	private String calComentario;
}