package cl.ufro.dci.perfecthostapi.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@DiscriminatorValue(value="Anfitrion")
@Table(name = "anfitrion")
public class Anfitrion extends Usuario {
	@OneToMany (cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "usu_id")
	private List<Anuncio> anuncios;
}