package cl.ufro.dci.perfecthostapi.domain;

import lombok.Data;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Transactional
@Table(name = "anuncio")
public class Anuncio {
	@Id
	@Column (name = "anu_id", nullable = false, length = 20)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long anuId;
	@OneToMany (cascade = CascadeType.ALL,orphanRemoval = true)
	@JoinColumn(name = "anu_id")
	private List<Reserva> reservas;
	@ManyToOne
	@JoinColumn(name = "usu_id")
	private Anfitrion anfitrion;
	@OneToMany (cascade = CascadeType.ALL,orphanRemoval = true)
	@JoinColumn(name = "anu_id")
	private List<Disponibilidad> fechasDisponibles;
	@OneToMany (cascade = CascadeType.ALL,orphanRemoval = true)
	@JoinColumn(name = "anu_id")
	private List<Cobros> cobros;
	@OneToMany (cascade = CascadeType.ALL,orphanRemoval = true)
	@JoinColumn(name = "anu_id")
	private List<Calificacion> calificaciones;
	@Column (name = "anu_ciudad",nullable = false)
	private String anuCiudad;
	@Column (name = "anu_descuento",nullable = true)
	private int anuDescuento;
	@Column (name = "anu_codigo_postal",nullable = false)
	private int anuCodigoPostal;
	@Column (name = "anu_direccion",nullable = false)
	private String anuDireccion;
	@Column (name = "anu_pais",nullable = false)
	private String anuPais;
	@Column (name = "anu_precio_dia", nullable = false)
	private int anuPrecioDia;
	@Column (name = "anu_numero_camas",nullable = false)
	private int anuNumeroCamas;
	@Column (name = "anu_bagno", nullable = false)
	private int anuBagno;
	@Column (name = "anu_cocina", nullable = false)
	private int anuCocina;
	@Column(name = "anu_permitir_fiesta")
	private boolean anuPermitirFiesta;
	@Column(name = "anu_internet")
	private boolean anuInternet;
	@Column(name = "anu_mascota")
	private boolean anuMascota;
	@Column (name = "anu_coordinacion_llegada", nullable = false)
	private String anuCoordinarLLegada;
	@Column (name = "anu_coordinacion_salida", nullable = false)
	private String anuCoordinarSalida;

	@Column (name = "anu_imagen", nullable = false)
	private String anuImagen;

	@Column(name = "anu_titulo", nullable=false)
	private String anuTitulo;
}