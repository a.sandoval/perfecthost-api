package cl.ufro.dci.perfecthostapi.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.*;
import javax.persistence.Table;

@Entity
@Data
@DiscriminatorValue(value="Admin")
@Table(name = "administrador")
public class Administrador extends Usuario{
}