package cl.ufro.dci.perfecthostapi.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@DiscriminatorValue(value="Huesped")
@Table(name = "huesped")
public class Huesped extends Usuario {
	@OneToMany (
		cascade = CascadeType.ALL,
		orphanRemoval = true
	)
	@JoinColumn(name = "usu_id")
	private List<Reserva> reservas;
	@OneToMany (
		cascade = CascadeType.ALL,
		orphanRemoval = true
	)
	@JoinColumn( name = "usu_id")
	private List<Calificacion> calificaciones;
}