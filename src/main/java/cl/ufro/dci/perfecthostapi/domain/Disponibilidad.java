package cl.ufro.dci.perfecthostapi.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "disponibilidad")
public class Disponibilidad {
	@Id
	@Column (
		name = "dis_id",
		nullable = false,
		length = 20
	)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long disId;
	@ManyToOne
	@JoinColumn(name = "anu_id")
	private Anuncio anuncio;
	@Column(name = "dis_inicio_disponible")
	private Date disInicioDisponible;
	@Column(name = "dis_fin_disponible")
	private Date disFinDisponible;
}