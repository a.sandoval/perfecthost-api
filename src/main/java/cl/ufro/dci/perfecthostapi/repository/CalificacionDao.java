package cl.ufro.dci.perfecthostapi.repository;


import cl.ufro.dci.perfecthostapi.domain.Calificacion;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface CalificacionDao extends CrudRepository<Calificacion, Long> {
}
