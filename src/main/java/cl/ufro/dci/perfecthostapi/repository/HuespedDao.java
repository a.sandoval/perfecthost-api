package cl.ufro.dci.perfecthostapi.repository;


import cl.ufro.dci.perfecthostapi.domain.Huesped;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface HuespedDao extends CrudRepository<Huesped, Long> {
}
