package cl.ufro.dci.perfecthostapi.repository;


import cl.ufro.dci.perfecthostapi.domain.Anuncio;
import cl.ufro.dci.perfecthostapi.domain.Disponibilidad;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface AnuncioDao extends CrudRepository<Anuncio, Long> {
    public List<Anuncio>findByFechasDisponibles(Disponibilidad disponibilidad);
    public List<Anuncio>findAll();
}
