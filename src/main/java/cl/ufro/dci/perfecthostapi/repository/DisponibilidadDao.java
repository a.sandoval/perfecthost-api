package cl.ufro.dci.perfecthostapi.repository;

import cl.ufro.dci.perfecthostapi.domain.Disponibilidad;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Date;

@Repository
@Transactional
public interface DisponibilidadDao extends CrudRepository<Disponibilidad, Long> {
    public Disponibilidad findByDisInicioDisponibleAndAndDisFinDisponible(Date dInicio, Date dFinal);
}
