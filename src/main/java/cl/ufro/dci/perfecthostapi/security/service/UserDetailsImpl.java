package cl.ufro.dci.perfecthostapi.security.service;

import cl.ufro.dci.perfecthostapi.domain.Administrador;
import cl.ufro.dci.perfecthostapi.domain.Anfitrion;
import cl.ufro.dci.perfecthostapi.domain.Huesped;
import cl.ufro.dci.perfecthostapi.domain.Usuario;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class UserDetailsImpl implements UserDetails {
	private static final long serialVersionUID = 1L;

	private Long id;

	private String username;

	@JsonIgnore
	private String password;

	private Collection<? extends GrantedAuthority> authorities;

	public UserDetailsImpl(Long id, String username, String password,
                           Collection<? extends GrantedAuthority> authorities) {
		this.id = id;
		this.username = username;
		this.password = password;
		this.authorities = authorities;
	}

	public static UserDetailsImpl build(Usuario user) {
		List<GrantedAuthority> authorities = null;
		if(user instanceof Anfitrion){
			authorities = Collections.singletonList(new SimpleGrantedAuthority(Anfitrion.class.getSimpleName()));
		}else if(user instanceof Huesped){
			authorities = Collections.singletonList(new SimpleGrantedAuthority(Huesped.class.getSimpleName()));
		}else if(user instanceof Administrador){
			authorities = Collections.singletonList(new SimpleGrantedAuthority(Administrador.class.getSimpleName()));
		}

		return new UserDetailsImpl(
				user.getUsuId(),
				user.getUsuNombre(),
				user.getUsuClave(),
				authorities);
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	public Long getId() {
		return id;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}