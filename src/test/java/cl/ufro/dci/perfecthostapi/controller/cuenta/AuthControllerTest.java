package cl.ufro.dci.perfecthostapi.controller.cuenta;

import cl.ufro.dci.perfecthostapi.domain.Anfitrion;
import cl.ufro.dci.perfecthostapi.domain.Huesped;
import cl.ufro.dci.perfecthostapi.domain.Usuario;
import cl.ufro.dci.perfecthostapi.security.dto.AuthenticationRequest;
import cl.ufro.dci.perfecthostapi.service.cuenta.UserServiceImp;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class AuthControllerTest {
    @Autowired
    private UserServiceImp userService;

    @Autowired
    private AuthController controller;

    @Autowired
    private PasswordEncoder encoder;

    boolean changeUser = false;

    @BeforeEach
    void setUp() {
        Usuario user = new Huesped();
        user.setUsuNombre("usuario1test");
        user.setUsuClave(encoder.encode("password"));
        userService.registrarUsuario(user);
    }

    @AfterEach
    void shutdown(){
        if (this.changeUser == false) {
            userService.borrarCuenta(userService.findByUsername("usuario1test").get().getUsuId());
        }
    }



    @AfterEach
    public void cleanup(){
        System.out.println("Test Finalizado");
        // Cleanup the database after each test
        userService.deleteAll();
    }
    @Test
    @DisplayName("Autentificacion")
    void authenticateUser() {
        AuthenticationRequest authenticationRequest = new AuthenticationRequest("usuario1test","passwordd");

        Assertions.assertThrows(BadCredentialsException.class, () -> {
            controller.authenticateUser(authenticationRequest);
        });
    }

    @Test
    @DisplayName("Obtener cuenta segun el id")
    void getFindById() {
        Usuario userEsperado = userService.findById(2L);
        Assertions.assertNull(userEsperado);
    }

    @Test
    @DisplayName("Cambiar rol")
    void cambioRol() {
        Usuario user = userService.findByUsername("usuario1test").get();
        Usuario userChangeRol = new Anfitrion();
        userChangeRol.setUsuNombre(user.getUsuNombre());
        userChangeRol.setUsuClave(user.getUsuClave());

        userService.borrarCuenta(user.getUsuId());
        userService.registrarUsuario(userChangeRol);

        Assertions.assertTrue(userService.findByUsername("usuario1test").get() instanceof Anfitrion, "Cambio de rol generado");
    }

    @Test
    @DisplayName("Obtener todos los usuarios")
    void listarUser() {
        Iterable<Usuario> iterable = userService.listarCuentas();
        List<Usuario> usuariosBD = new ArrayList<>();
        iterable.forEach(usuariosBD::add);

        Assertions.assertEquals(1,usuariosBD.size());
    }

    @Test
    @DisplayName("Actualizar usuario")
    void update() {
        Usuario userActual = userService.findByUsername("usuario1test").get();
        AuthenticationRequest updateUser = new AuthenticationRequest("usuario1testupdate", "passupdate");

        userActual.setUsuNombre(updateUser.getUsername());
        userActual.setUsuClave(encoder.encode(updateUser.getPassword()));

        userService.registrarUsuario(userActual);

        this.changeUser = true;
        Assertions.assertTrue(userService.findByUsername("usuario1testupdate").isPresent());
    }

    @Test
    @DisplayName("Borrar usuario")
    void delete() {
        userService.borrarCuenta(1L);
        this.changeUser = true;
        Assertions.assertNull(userService.findById(1L));
    }

    @Test
    @DisplayName("Recuperar usuario")
    void recuperate() {
        Usuario userActual = userService.findByUsername("usuario1test").get();
        String passnew = "passwordrecuperate";

        userActual.setUsuClave(encoder.encode(passnew));

        userService.registrarUsuario(userActual);

        Assertions.assertTrue(encoder.matches(passnew, userService.findByUsername("usuario1test").get().getUsuClave()));
    }

    @Test
    @DisplayName("Registrar anfitrion")
    void registrarAnfitrion() {
        Usuario user = new Anfitrion();
        user.setUsuNombre("testAnfitrion");
        user.setUsuClave(encoder.encode("password"));
        userService.registrarUsuario(user);

        Usuario userEsperado = userService.findByUsername(user.getUsuNombre()).get();


        Assertions.assertTrue(userEsperado instanceof Anfitrion);
    }

    @Test
    @DisplayName("Registrar huesped")
    void registrarHuesped() {
        Usuario user = new Huesped();
        user.setUsuNombre("testHuesped");
        user.setUsuClave(encoder.encode("password"));
        userService.registrarUsuario(user);

        Usuario userEsperado = userService.findByUsername(user.getUsuNombre()).get();

        Assertions.assertTrue(userEsperado instanceof Huesped);
    }
}